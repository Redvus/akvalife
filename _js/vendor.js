; (function () {

	function scrollbarDesignSmooth() {
		$('.design-content').niceScroll({
			cursorcolor: "#000000",
			cursorwidth: "5px",
			cursorborder: "1px solid #fff",
			scrollspeed: 100,
			mousescrollstep: 12
		});
	}

	function scrollbarSidebarSmooth() {
		$('.design-sidebar').niceScroll({
			cursorcolor: "#000000",
			cursorwidth: "5px",
			cursorborder: "1px solid #fff",
			scrollspeed: 100,
			mousescrollstep: 40
		});
	}

	// scrollbarDesignSmooth();
	// scrollbarSidebarSmooth();

})(jQuery);